<?php 


class Api_controller extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Api_model');
		date_default_timezone_set('Asia/Karachi');
	}

	public function user_login()
	{

        $email           = $this->input->post('email');
        $password       = md5($this->input->post('password'));
    
        $result = $this->Api_model->do_chk_user($email,$password);
        
        
        if($result === false)
        {
            $data['error']  = True;
            $data['msg']    = "Cresentials Not Matched.";
            echo json_encode($data); 
        }
        else
        {
            $this->db->set('last_login', date('Y-m-d H:i:s'))->where('id',$result->id)->update('user'); 
            $data['error']      = False;
            $data['msg']        = 'Login Succesfully!';
            $data['userdata']  = $result;
            echo json_encode($data);
        }
	}



	
}


 ?>