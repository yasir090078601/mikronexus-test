<?php
if(! function_exists('get_constants'))
{
	function get_constants($param)
	{
		$constants = array(
			"USER_TYPES" => array('Vendor' => "vendor",'Buyer' => "buyer" ),
			"ATTACHMENT_OWNER_TYPES" => array("CATEGORY"=>"category","SERVICE"=>"service","USER"=>"user","USER_CATEGORY"=>"user_category","USER_SERVICE"=>"user_service"),
			"QUESTION_OWNER_TYPES" => array('Category' => "category" )
		);
		return $constants[$param];
	}
}
?>