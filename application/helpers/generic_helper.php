<?php
 if (!function_exists('image_upload')) {
     function image_upload($file_name,$entity)
     {
        $CI = &get_instance();
        $config['upload_path']        =  'assets/admin_assets/'.$entity.'/';
        $config['allowed_types']      =  'gif|jpg|png|jpeg|pdf|docx|xlsx|xls|ppt';
        $CI->load->library('upload', $config);
        if($CI->upload->do_upload($file_name))
        {
            return array("status"=>true,"file_name"=>$CI->upload->data('file_name'));
        }
        else
        {
            return array("status"=>false);
        } 
        
    }
}

if(!function_exists('rename_file')) {
    function rename_file($file_name)
	{
        $file_name_info=pathinfo($file_name);
		$file_extension=$file_name_info['extension'];
		$file_name=$file_name_info['filename'];
		$curr_date_time=strtotime(date("Y:m:d H:i:s"));
		$new_file_name=$curr_date_time."--".$file_name.".".$file_extension;
		return $new_file_name;
	}
}



if (!function_exists('date_time_converter')) {
    function date_time_converter($old_date,$format)
    {
        $old_date_timestamp = strtotime($old_date);
        $new_date = date($format, $old_date_timestamp);
        return $new_date;
    }
}

if(!function_exists('generic_email'))
{
    function generic_email($to,$subject,$message)
    {
        
        $CI =& get_instance();
        
        $CI->email->from('info@wedmubarak.com', 'Wedding Service');
        $CI->email->to($to);
                
        $CI->email->subject($subject);
        $CI->email->message($message);
        
                
        if($CI->email->send())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

if(!function_exists('current_date'))
{
    function current_date()
    {
        return date('Y-m-d H:i:s');
    }
}