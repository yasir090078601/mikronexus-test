<?php 
if(!function_exists('check_user_session'))
{
	function check_user_session()
	{
		$CI =& get_instance();
		if(!$CI->session->userdata('logged_in_user_data'))
		{
			return redirect(base_url());
		}
	}
}
?>