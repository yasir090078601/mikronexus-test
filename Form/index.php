<!DOCTYPE html>
<html>
<head>
	<title>FORM</title>
	<link rel="stylesheet" type="text/css" href="bootstrap.css"> 
</head>
<body>
	<div class="container-fluid text-center p-4" style="background-color: #99b3e6;color:#fff"><h3>AJAX FORM</h3></div>

	<div class="container p-4">
		<div class="row mt-5">
		<div class="col-md-4"></div>
		<div class="col-md-4"> 

			<div class="form_msg"></div>
			<div class="form-group">
  				<label for="emails">Email:</label>
				<input type="email" id='emails' name="email" placeholder="abc@xyz.com" class="form-control mb-4 email">
			</div>
			<div class="form-group">
  				<label for="pwd">Password:</label>
				<input type="password" id="pwd" name="pwd" placeholder="******" class="form-control mb-4 pwd">
			</div>
			<button class="btn btn-primary submit form-control">Login</button> 
	</div>
	</div>
	</div>
	<script type="text/javascript" src="jquery-3.3.1.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(".submit").click(function(){

			var dataPost = {
			   "email": $(".email").val(),
			   "pwd" :  $(".pwd").val()
			};

			var dataString = JSON.stringify(dataPost);
		
			$.ajax({
			   url: 'server.php',
			   data: {myData: dataString},
			   type: 'POST',
			   success: function(response) {
			     $(".form_msg").html(response);
			   }
			});
		})
	})
</script>


</body>
</html>


